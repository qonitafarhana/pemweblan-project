@extends('layouts.appp')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        {{-- <div class="col-md-8"> --}}
            <div class="card">
                <div class="card-header">{{ __('Katalog') }}</div>

                <div class="card-body">
                    {{-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }} --}}

                    <div class="container">
                        <div class="row justify-content-center">
                           <div class="col-md-12 mb-4">
                              <img src="{{ url('images/logo.png')}}" class="rounded mx-auto d-block" width="500" alt="">
                           </div>
                           @foreach($product as $product)
                           <div class="col-md-4">
                              <div class="card">
                                 <img src="images/{{$product->image}}" class="card-img-top" alt="...">
                                <div class="card-body">
                                  <h5 class="card-title"></h5>
                                   <br class="card-text">
                                   <strong>{{$product->product}}</strong>
                                   <br>
                                   <strong>Harga :</strong> Rp. {{ number_format($product->price) }}

                                   </p>
                                   <a href="#" class="btn btn-primary"><i class="fas fa-add-to-cart"></i>Beli</a>
                                </div>
                              </div>
                           </div>
                           @endforeach





<div class="mt-5">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Kota</th>
                <th>Cabang</th>
                <th width="15%" class="text-center">Jumlah Cabang</th>
            </tr>
        </thead>
        <tbody>
            @foreach($kota as $a)
            <tr>
                <td>{{ $a->kota }}</td>
                <td>
                    @foreach($a->toko as $t)
                        {{$t->toko}}
                        <br>
                        {{$t->alamat}}
                        <br>
                        No. telp : {{$t->telp}}
                        <br>
                        <br>
                    @endforeach
                </td>
                <td class="text-center">{{ $a->toko->count() }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
@endsection
